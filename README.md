# **Gestio de Fitxers en c** #
### projecte Gestio de Fitxers  Alta-Baixa-llista per pantalla-llista web-llista Joc d'un Joc Concreta.
---
la instrucció:

en el meu Project consisteix a realitzar la Alta Baixa Llista per pantalla i llista WEB dels Jocs 

el Projecte mostra un menu i me demane tria una opcio del menu qui jo vol fer per exemple com me mostre en la captura següent 



### Menu el que mostre la meva practica 

![execusion Menu](images/menu.png)

---

### **Alta**

l'accio alta lo qui fa es me demane tots els informacions del Joc 

---
 la primera part del meu practica es la decalracio dels variable com mostre els següent codi font 
i després tinc qui obrir un fitxer per guardar tots els informacions del joc qui tinc qui dona de alta 
I després el qui fa és em demana les dades del Joc qui tinc al struct i el do while el qui fa és si l'usuari vol fer més altes dels Jocs per facilitar el programa.

```c
int Alta(){
    struct Jocs j;
    FILE *fitxer;
    int error=0;

    char sn;
    bool mesaltes=true;


    //obrir fitxer escriptura
    fitxer= fopen("jocs.bin","ab");//fitxer binari (b) d'afegir (a)
    if(fitxer==NULL){
        error=1; //error obrir fitxer
    }
    else{
        //for(int i=0; i<nJocs; i++){
        while(mesaltes==true){
            entradaJoc(&j);
            ecriureJocDisc(fitxer, &j, 1);

            do{
                printf("vols fer Mes altes (s/n)");
                scanf("%c",&sn);BB;
                if(sn=='s')mesaltes=true;
                else{
                    if(sn=='n'){
                            mesaltes=false;
                    }
            }
            }while(sn!='s' && sn!='n');
         
                }
            }


    if(fclose(fitxer)){
        printf("error al tancar el fitxer...");
    }
    return error;

}
```
Per funciona perfectament l'alta cal fer l'acció d'entrada dels Jocs i el qui fa es me demana les dades del Joc
```c
void entradaJoc(struct Jocs *j){
    char sn;
    printf("\nIMPORTANT ⚠️: el Id del Joc es el Clau no hi ha de ser Repetit \n");
    printf("id del Joc:\n\t");
    scanf("%d",&j->id);BB;
    printf("titol del Joc:\n\t");
    scanf("%35[^\n]",j->titol);BB;// (*p).nom
    printf("es Multi Jugador:(s->si/n->no)\n\t");
    scanf("%c",&sn);BB;
    if(sn=='s')j->esMultiJugador=true;
    else{
        if(sn=='n'){
            j->esMultiJugador=false;
        }
        else printf("\nIMPORTANT ⚠️: el caracter qui has introduit no es ni 'n' ni 's' \n");
    }
    printf("\nanyPublicacio:\n\t");
    scanf("%i",&j->anyPublicacio);BB;// (*p).poblacio


}

```
- execució Alta  
quant tria l'suari la opcio 1 lo qui fa es demana dades del Joc i quant acabe la Alta me neteja tota la pantalla i torna al Menu
![captura Alta](images/execusioAlta.png)
---  
### **llistat de tots els jocs per Pantalla**
---
En l'opció del meu menú quan l'usuari tria l'opció 2 mostri totes les informacions dels Jocs qui hi ha al fitxer a un guarden les dades els Jocs
instrucció:
en el codi font l'acció la primera etapa el qui fa és
obrir el fitxer de format Read binari i el condicional el qui fa és en Cas de l'usuari tria l'opció de llista tots els Jocs qui estan al fitxer surt a la pantalla qui no hi ha cap registre I l'estructura iterativa el qui fa es controla el fitxer per no passi al final del fitxer i després anna llegir els Jocs qui hi ha al fitxer si l'acció retorna 0 si hi ha algun registre
i si no marcat esborrat (d'una altra manera el Joc no està en baixa)i l'última part es controla els errors qui poden sortir al meu codi per controlar i depres tanca el fitxer.
```c
int generallistapantalla(){
    FILE *fitxer;
    int error,n;
    struct Jocs j;
    int i=1;
    printf("\n***LECTURA DE JOCS DEL FITXER****\n");
    //obrir llegir escriptura
    fitxer= fopen("jocs.bin","rb");//fitxer binari (b) d'afegir (a)
    if(fitxer==NULL){
        error=1; //error obrir fitxer
    }
    if(feof(fitxer))printf("no hi ha cap registre Al Fitxer\n");
    while(!feof(fitxer)){
        n=llegirJocsDisc(fitxer, &j, 1);
        if(n==0){

            if(j.marcaEsborrat!='*'){
                    printf("******Dades de '%i' Joc\n",i);
                    imprimirJoc(j);
                    i++;
            }

        }
        if(n==1){
            printf("\nError en obrir fitxer...\n");
            break;
        }
        if(n==3){
            printf("\nError de lectura...\n");
            break;
        }
    }
    if(fclose(fitxer)){
        printf("error al tancar el fitxer...");
    }
    return error;
}

```
per funcciona el codi perfectament nessistem un accio de imprimir els jocs una a una  
**El codi de la accio de imprimir els Jocs:**
```c
void imprimirJoc(struct Jocs j){
    printf("ID del Joc:\n\t'%d'\n",j.id);
    printf("titol:\n\t'%s'\n",j.titol);
    printf("es Multi Jugador:\n\t");
    if(j.esMultiJugador==true){
        printf("'Si'\n");
    }
    else printf("'No'\n");
    printf("any Publicacio:\n\t'%i'\n", j.anyPublicacio);
}
```
---
### execució del la opcio per llistar tots els Jocs qui tinc al meu fitxer

![execució del meu codi](images/execusiodellistatpantalla.png)


---
### llistar tots els Jocs en WEB

En la funció de llistar els Jocs en web
la primera part és la declaració dels Variables
el primer variable es cap de l'HTML i lo qui fa
el cap és posar els bases de l'HTML i obrir la taula,el variable Cos servi per assignar continguts de les informacions de tots els Jocs qui hi ha al fitxer i la resta de la taula i el peu lo qui te es el tancament del body de l'html i el tancament de l'html .
```c


    //aquest variable guarda el cap  del Html
    char cap[]="<!DOCTYPE html>\n\
            <html lang=\"en\">\n\
            <head>\n\
                <meta charset=\"UTF-8\">\n\
                <title>Document</title>\n\
                <link rel=\"stylesheet\" href=\"style.css\">\
            </head>\n\
            <body>\n\
            <br>\
            <br>\
            <br>\
            <table border=1 class=\"center\">\
            <tr><th>ID</th><th>Titol</th> <th>any Publicacio</th> <th>es Multi Jugador</th></tr>";
    //aquest variable guarda el Cos  del Html
    char cos[MAX_COS+1]="";
    //aquest variable guarda el Pru  del Html
    char peu[]="\n</table></body>\n</html>";
    int n;
    int error;
    FILE *fitxer;
    FILE *fitxer2;
    struct Jocs j;
```
i la següent etapa és obrir el fitxer qui té els Jocs de Forma de lectura
i el fitxer HTML de forma escriptura per no em repeteix el codi d'HTML al meu fitxer HTML
```c
 fitxer=fopen("jocs.bin","rb");
    if(fitxer==NULL){
        error=1;
    }
    fitxer2=fopen("index.html","w");
    if(fitxer2==NULL){
        error=1;
    }
```
després es posar cap al fitxer d'HTML amb la funció fputs 
```c
error=fputs(cap,fitxer2);
    if(error==EOF)
    {
        error=2;
    }
```
ara per posar les dades dels Jocs al meu fitxer tinc qui fer una iteració qui Quan el Fitxer Jocs Arribi al Final del Fitxer Surt,i després tinc una acció llegir les dades dels Jocs una a una i quant tinc les dades posar-ho al meu Cos de l'HTML
```c
 //aquesta iteracion es per Quant el Fitxers Jocs Arribi al Final  del Fitxer Surt
    while(!feof(fitxer)){
        //aquesta accion es per llegir el jocs dins del Fitxe i posa els informacions de una Jocs a j
        n=llegirJocsDisc(fitxer, &j, 1);
        if(n==0){
            //aquest es accion me returone el cos amb els informacions del Joc qui esta Guardat
            llistaWeb(fitxer,cos,j);
        }
        if(n==1){
            printf("\nError en obrir fitxer...\n");
            break;
        }
        if(n==3){
            printf("\nError de lectura...\n");
            break;
        }
    }

    
    error=fputs(cos,fitxer2);
    if(error==EOF){
        error=2;
    }
    //aquesta accio posa
    error=fputs(peu,fitxer2);
    if(error==EOF){
        error=2;
    }
    //tancar els fitxers
    if(fclose(fitxer2)){
        error=3;
    }
    if(fclose(fitxer)){
        printf("error al tancar el fitxer...");
    }
```
i per funcionar perfectament el llista WEB cal fer una acció lo qui fa es posar les dades dels Jocs al cos i el condicional marca esborrat es quan l'usuari ef ficar a un Joc Baixa controla per no posar-la al llista web
```c
void llistaWeb(FILE *fitxer,char cos[],struct Jocs j){
    char cadenaTmp[100];
    //generar taula amb dades
    if(j.marcaEsborrat!='*'){
        strcat(cos,"<tr><td>");
        sprintf(cadenaTmp,"%d",j.id);
        strcat(cos,cadenaTmp);
        strcat(cos,"</td><td>");
        strcat(cos,j.titol);
        strcat(cos,"</td><td>");
        sprintf(cadenaTmp,"%d",j.anyPublicacio);//convertir numero a cadena
        strcat(cos,cadenaTmp);
        strcat(cos,"</td><td>");
        if(j.esMultiJugador==true){
            strcat(cos,"Si");
        }
        else strcat(cos,"No");
        strcat(cos,"</td></tr>");
    }
}
``` 
####  una prova d'execució de llistat WEB 
la primera cosa es tria l'opció del llistat per WEB i el següent part ens anem al fitxer de l'HTML per mostrar totes les informacions qui te
![opcio](images/execusiollistaWeb.png)
![opcio](images/web.png)

---
### **Consulta d'un Joc Concreta**
---
la funció llista concreta lo qui fa es mostra les informacions del Joc qui Vols l'usuari per una cerca amb el camp clau i per imprimir aquest Joc necessitem comprova amb un condicional si aquest Joc existeix i no està marcat Esborrat i si hi ha aquest Joc el-mostri per pantalla amb la seva dada
 
 
```c
int llistaJocconcreta(){
    int id;
    struct Jocs j;
    FILE *f;
    int n;
    f=fopen("jocs.bin","rb+");
    if(f==NULL){
        printf("Error en obrir el fitxer\n");
        return -1;
    }
    printf("\nIntrodueix el 'ID' del Joc a cercar? ");
    scanf("%d",&id);BB;
    while(!feof(f)){
        n=fread(&j,sizeof(struct Jocs),1,f);
        if(!feof(f)){
            if(n==0){
                printf("Error de lectura");
                return -3;
            }
        //fitxer llegit correctament.
        //si ( no borrat I trobat? ) Llavors 
            if(j.marcaEsborrat!='*' && (j.id==id) ){
            imprimirJoc(j);
            }
        }
    }
        fclose(f);

    return 0;
}
```
---

#### ***una execusio del llistat joc concreta***

el primer es tria l'opció de llistar un Joc concreta i després buscar per al Joc amb el camp clau i en el meu cas el camp Clau es el codi .
![image llista concreta](images/execusioJocsconcreta.png)  

---
![image llis](images/llistaconcreta.png)

---
### ***Baixa***

---
En el codi c la primera cosa és obrir el meu fitxer de forma de lectura
i després controla l'error del cas qui no trobe el fitxer i demano a l'usuari qui em dona l'ID del Joc per a qui vol fer la baixa i el condicional controlar qui l'ID qui ha introduït per al usuari i qui no està donat de baixa abans,i despres preguntarle si vol fe la baixa o no.
```c

int baixa(){

    int id;
    struct Jocs j;
    FILE *f;
    int n;
    char sn;
    f=fopen("jocs.bin","rb+");
    if(f==NULL){
        printf("Error en obrir el fitxer\n");
        return -1;
    }
    printf("\nIntrodueix el 'ID' del Joc a cercar? ");
    scanf("%d",&id);BB;
    while(!feof(f)){
        n=fread(&j,sizeof(struct Jocs),1,f);
        if(!feof(f)){
            if(n==0){
                printf("Error de lectura");
                return -3;
            }
        //fitxer llegit correctament.
        //si ( no borrat I trobat? ) Llavors codi per esborrar el registre
            if(j.marcaEsborrat!='*' && (j.id==id) ){

            imprimirJoc(j);
            printf("segu Vols fer la Baixa d'aquesta Joc(s/n):");
            scanf("%c",&sn);BB;
            if(sn=='s'){
            //-1 saltem un endarrere perquè l’ha llegit i ja estem al següent registre!!
                if( fseek(f, -(long) sizeof(struct Jocs), SEEK_CUR ) ) return -2;
            //marcar com esborrat
                j.marcaEsborrat='*';
            //escriure al disc
                n=fwrite(&j, sizeof(struct Jocs), 1, f);
                //control d’error d’escriptura
                if(n!=1){
                    printf("\nBaixa: Error d'escriptura");
                    return -2;
                }
           //sortir del while(!eof(f)) (break)
                break;
            }else {
                if(sn=='n')break;
                else {
                        printf("\nIMPORTANT :\" el caracter qui has introduit no es ni 'n' ni 's'\n i per seguretat no se fa la baixa al Joc\" \n");
                    }
                }
            }
        }
    }
        fclose(f);
return 0;
}
``` 
una execusio de la Baixa 
![baixa](images/baixa.png)



 






