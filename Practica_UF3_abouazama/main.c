#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>


#define MAX_COS 10000
#define MAX_NOM 35
#define MAX_TITOL 100
#define MAX_POBLACIO 50
#define BB while(getchar()!='\n')

struct Jocs
{
    char titol[MAX_TITOL+1];
    bool esMultiJugador;
    int anyPublicacio;
    int id;///el Clau del Joc
    char marcaEsborrat;
};
void entradaJoc(struct Jocs *j);
void imprimirJoc(struct Jocs j);
int ecriureJocDisc(FILE *fitxer,struct Jocs *j, int nReg);
int llegirJocsDisc(FILE *fitxer,struct Jocs *j, int nReg);
void llistaWeb(FILE *fitxer,char cos[],struct Jocs j);
int generallistaWeb();
int generallistapantalla();
int Alta();
void triaopcio(int *opcio);
void menu();
void feopcio(int opcio);
int baixa();
void parafinsquidemaneelusuariaseguir();
void netejarpantalla();


int main()
{


    int error=0;
    int opcio;



    do
    {
        menu();
        triaopcio(&opcio);
        feopcio(opcio);
        parafinsquidemaneelusuariaseguir();

    }while(opcio!=9);


    return error;
}
int llistaJocconcreta(){
    int id;
    struct Jocs j;
    FILE *f;
    int n;

    f=fopen("jocs.bin","rb+");
    if(f==NULL){
        printf("Error en obrir el fitxer\n");
        return -1;
    }
    printf("\nIntrodueix el 'ID' del Joc a cercar? ");
    scanf("%d",&id);BB;
    while(!feof(f)){
        n=fread(&j,sizeof(struct Jocs),1,f);
        if(!feof(f)){
            if(n==0){
                printf("Error de lectura");
                return -3;
            }
        //fitxer llegit correctament.
        //si ( no borrat I trobat? ) Llavors codi per esborrar el registre
            if(j.marcaEsborrat!='*' && (j.id==id) ){

            imprimirJoc(j);
            }
        }
    }

        fclose(f);

    return 0;
}
void parafinsquidemaneelusuariaseguir(){

    char stop=' ';
    //en cas de Windows
    //system("pause");
    ///aquest es per els dos linux i windows
    printf("\nPrem enter per seguir si us Plau ...\n");
    scanf("%c",&stop);
    netejarpantalla();

}
void netejarpantalla(){
    //system("clear");//aquest es per linux
    system("cls");//aquest es per WINDOWS
}
void feopcio(int opcio){
    switch(opcio)
    {
    case 1:
        Alta();
        break;

    case 2:
        generallistapantalla();
        break;
    case 3:
        generallistaWeb();
        break;
    case 4:
        llistaJocconcreta();
        break;
    case 5:
        baixa();
        break;

    }
}

void triaopcio(int *opcio){
    printf("Opcio(1-4) i per Sortir 9: ");
    scanf("%i",&*opcio);BB;
}

void menu(){


    printf("\t\t   1- Alta                  \n");
    printf("\t\t   2- Llista de tots els jocs per pantalla  \n");
    printf("\t\t   3- Llistar tots els jocs per web       \n");
    printf("\t\t   4- Llista un Joc concreta \n");
    printf("\t\t   5- Baixa                 \n");

    printf("\t\t   9- sortir                \n");




}

int Alta(){
    struct Jocs j;
    FILE *fitxer;
    int error=0;

    char sn;
    bool mesaltes=true;



    //obrir fitxer escriptura
    fitxer= fopen("jocs.bin","ab");//fitxer binari (b) d'afegir (a)
    if(fitxer==NULL){
        error=1; //error obrir fitxer
    }
    else{

        while(mesaltes==true){
            entradaJoc(&j);
            ecriureJocDisc(fitxer, &j, 1);

            do{
                printf("vols fer Mes altes (s/n)");
                scanf("%c",&sn);BB;
                if(sn=='s')mesaltes=true;
                else{
                    if(sn=='n'){
                            mesaltes=false;
                    }
            }
            }while(sn!='s' && sn!='n');

                }
            }


    if(fclose(fitxer)){
        printf("error al tancar el fitxer...");
    }
    return error;

}
///aquesta accio genera tots els Jocs qui hi ha al meu fitxer per patalla
int generallistapantalla(){
    FILE *fitxer;
    int error,n;
    struct Jocs j;
    int i=1;
    printf("\n***LECTURA DE JOCS DEL FITXER****\n");
    //obrir llegir escriptura
    fitxer= fopen("jocs.bin","rb");//fitxer binari (b) d'afegir (a)
    if(fitxer==NULL){
        error=1; //error obrir fitxer
    }
    if(feof(fitxer))printf("no hi ha cap registre Al Fitxer\n");
    while(!feof(fitxer)){
        n=llegirJocsDisc(fitxer, &j, 1);
        if(n==0){

            if(j.marcaEsborrat!='*'){
                    printf("******Dades de '%i' Joc\n",i);
                    imprimirJoc(j);
                    i++;
            }

        }
        if(n==1){
            printf("\nError en obrir fitxer...\n");
            break;
        }
        if(n==3){
            printf("\nError de lectura...\n");
            break;
        }
    }
    if(fclose(fitxer)){
        printf("error al tancar el fitxer...");
    }
    return error;
}
int generallistaWeb(){
    //aquest variable guarda el cap  del Html
    char cap[]="<!DOCTYPE html>\n\
            <html lang=\"en\">\n\
            <head>\n\
                <meta charset=\"UTF-8\">\n\
                <title>Document</title>\n\
                <link rel=\"stylesheet\" href=\"style.css\">\
            </head>\n\
            <body>\n\
            <br>\
            <br>\
            <br>\
            <table border=1 class=\"center\">\
            <tr><th>ID</th><th>Titol</th> <th>any Publicacio</th> <th>es Multi Jugador</th></tr>";
    //aquest variable guarda el Cos  del Html
    char cos[MAX_COS+1]="";
    //aquest variable guarda el Pru  del Html
    char peu[]="\n</table></body>\n</html>";
    int n;
    int error;
    FILE *fitxer;
    FILE *fitxer2;
    struct Jocs j;
    //aquesta part es per Obrir el fitxer qui te formacions dels Jocs
    fitxer=fopen("jocs.bin","rb");
    if(fitxer==NULL){
        error=1;
    }
    //aquesta part es per Obrir el fitxer HTml per posar els Jocs al fitxer desde el fitxer JOcs
    fitxer2=fopen("index.html","w");
    if(fitxer2==NULL){
        error=1;
    }
    //aquesta part es posa el cap del html al fitxer de Html
    error=fputs(cap,fitxer2);
    if(error==EOF)
    {
        error=2;
    }
    //aquesta iteracion es per Quant el Fitxers Jocs Arribi al Final  del Fitxer Surt
    while(!feof(fitxer)){
        //aquesta accion es per llegir el jocs dins del Fitxe i posa els informacions de una Jocs a j
        n=llegirJocsDisc(fitxer, &j, 1);
        if(n==0){
            //aquest es accion me returone el cos amb els informacions del Joc qui esta Guardat
            llistaWeb(fitxer,cos,j);
        }
        if(n==1){
            printf("\nError en obrir fitxer...\n");
            break;
        }
        if(n==3){
            printf("\nError de lectura...\n");
            break;
        }
    }

    //aquesta es posar el informacions de tots dels Jocs al fitxer de Html
    error=fputs(cos,fitxer2);
    if(error==EOF){
        error=2;
    }
    //aquesta accio posa
    error=fputs(peu,fitxer2);
    if(error==EOF){
        error=2;
    }
    //tancar els fitxers
    if(fclose(fitxer2)){
        error=3;
    }
    if(fclose(fitxer)){
        printf("error al tancar el fitxer...");
    }


    return error;

}


void entradaJoc(struct Jocs *j){
    char sn;
    printf("\nIMPORTANT : el Id del Joc es el Clau no hi ha de ser Repetit \n");
    printf("id del Joc:\n\t");
    scanf("%d",&j->id);BB;
    printf("titol del Joc:\n\t");
    scanf("%35[^\n]",j->titol);BB;// (*p).nom
    printf("es Multi Jugador:(s->si/n->no)\n\t");
    scanf("%c",&sn);BB;
    if(sn=='s')j->esMultiJugador=true;
    else{
        if(sn=='n'){
            j->esMultiJugador=false;
        }
        else printf("\nIMPORTANT : el caracter qui has introduit no es ni 'n' ni 's' \n");
    }
    printf("\nany Publicacio:\n\t");
    scanf("%i",&j->anyPublicacio);BB;// (*p).poblacio
    j->marcaEsborrat=' ';


}

void imprimirJoc(struct Jocs j){
    printf("ID del Joc:\n\t'%d'\n",j.id);
    printf("titol:\n\t'%s'\n",j.titol);
    printf("es Multi Jugador:\n\t");
    if(j.esMultiJugador==true){
        printf("'Si'\n");
    }
    else printf("'No'\n");
    printf("any Publicacio:\n\t'%i'\n", j.anyPublicacio);
}

int ecriureJocDisc(FILE *fitxer,struct Jocs *j, int nReg){
    int n,error=0;
    //escriure les dades de la persona al fitxer persones.bin
    n=fwrite(j,sizeof(struct Jocs),nReg,fitxer);
    if(n!=nReg){
        error=2; //error numero registres escrits
    }
    return error;
}
/**aquesta part genera el cos amb els
informacions dels Jocs qui tinc al meu
fitxer */
void llistaWeb(FILE *fitxer,char cos[],struct Jocs j){


    char cadenaTmp[100];


    //generar taula les de la taula
    if(j.marcaEsborrat!='*'){
        strcat(cos,"<tr><td>");
        sprintf(cadenaTmp,"%d",j.id);
        strcat(cos,cadenaTmp);
        strcat(cos,"</td><td>");
        strcat(cos,j.titol);
        strcat(cos,"</td><td>");
        sprintf(cadenaTmp,"%d",j.anyPublicacio);//convertir numero a cadena
        strcat(cos,cadenaTmp);
        strcat(cos,"</td><td>");
        if(j.esMultiJugador==true){
            strcat(cos,"Si");
        }
        else strcat(cos,"No");
        strcat(cos,"</td></tr>");
    }



}


/**
Retorn de tipus enter. Interpretació:
    0 -> no error. Hi ha les dades llegides del fitxer al paràmetre *p
    1 -> error obrir fitxer
    3 -> error de lectura
    4 -> Advertència. Ha arribat al final del fitxer.
*/
int llegirJocsDisc(FILE *fitxer,struct Jocs *j, int nReg){
    int n,error=0;
    //escriure les dades de la persona al fitxer persones.bin
    n=fread(j,sizeof(struct Jocs),nReg,fitxer);
    if(!feof(fitxer)){
        if(n==0){
                printf("\nerror de lectura\n");
            error=3; //error de lectura
        }
    }
    else{
        error=4 ; //És un avís. Ha arribat al final del fitxer.
    }
    return error;
}

int baixa(){

    int id;
    struct Jocs j;
    FILE *f;
    int n;
    char sn;
    f=fopen("jocs.bin","rb+");
    if(f==NULL){
        printf("Error en obrir el fitxer\n");
        return -1;
    }
    printf("\nIntrodueix el 'ID' del Joc a cercar? ");
    scanf("%d",&id);BB;
    while(!feof(f)){
        n=fread(&j,sizeof(struct Jocs),1,f);
        if(!feof(f)){
            if(n==0){
                printf("Error de lectura");
                return -3;
            }
        //fitxer llegit correctament.
        //si ( no borrat I trobat? ) Llavors codi per esborrar el registre
            if(j.marcaEsborrat!='*' && (j.id==id) ){

            imprimirJoc(j);
            printf("segu Vols fer la Baixa d'aquesta Joc(s/n):");
            scanf("%c",&sn);BB;
            if(sn=='s'){
            //-1 saltem un endarrere perquè l’ha llegit i ja estem al següent registre!!
                if( fseek(f, -(long) sizeof(struct Jocs), SEEK_CUR ) ) return -2;
            //marcar com esborrat
                j.marcaEsborrat='*';
            //escriure al disc
                n=fwrite(&j, sizeof(struct Jocs), 1, f);
                //control d’error d’escriptura
                if(n!=1){
                    printf("\nBaixa: Error d'escriptura");
                    return -2;
                }
           //sortir del while(!eof(f)) (break)
                break;
            }else {
                if(sn=='n')break;
                else {
                        printf("\nIMPORTANT :\" el caracter qui has introduit no es ni 'n' ni 's'\n i per seguretat no se fa la baixa al Joc\" \n");
                    }
                }
            }
        }
    }
        fclose(f);
return 0;
}
